

struct bintree {
    int value;
    struct bintree *left;
    struct bintree *right;
};


struct bintree *new_bintree(int value) {
    struct bintree *new = malloc(sizeof(struct bintree));
    new->value = value;
    new->left = NULL;
    new->right = NULL;
    return new;
}

void print_bintree(struct bintree *node) {
    if (node->left != NULL)
        print_bintree(node->left);
    printf("Node value: %d\n", node->value);
    if (node->right != NULL)
        print_bintree(node->right);
}

void walk_bintree(struct bintree *node, int *array, int *count) {
    if (node->left != NULL)
        walk_bintree(node->left, array, count);
    array[(*count)++] = node->value;
    if (node->right != NULL)
        walk_bintree(node->right, array, count);
}


void bintree_place(struct bintree *node, struct bintree *new_node) {
    if (new_node->value <= node->value) {
        if (node->left == NULL) {
            node->left = new_node;
        } else {
            bintree_place(node->left, new_node);
        }
    } else {
        if (node->right == NULL) {
            node->right = new_node;
        } else {
            bintree_place(node->right, new_node);
        }
    }
}


void bintree_sort(int *array, long length) {
    int i;
    struct bintree *root;
    struct bintree *new;

    root = new_bintree(array[0]);
    for (i = 1; i < length; i++) {
        new = new_bintree(array[i]);
        bintree_place(root, new);
    }
    int *count = calloc(sizeof(array), length);
    walk_bintree(root, array, count);
}
