/* Infrastructure for sorting arrays of ints. */

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include "insertion.c"
#include "bintree.c"
#include "heap.c"
#include "merge.c"
#include "quick.c"

#define LENGTH 100
#define LIMIT 1000
#define POWERS 6

enum complexity {
    N_SQ,
    N_LG_N,
    N,
    LG_N
};

enum sort_type {
    INSERTION,
    BIN_TREE,
    HEAP,
    MERGE,
    QUICK
};

/* O(n) */
void print_array(int * array, long length) {
    int i;
    for (i = 0; i < length - 1; i++) {
        printf("%d, ", array[i]);
    }
    printf("%d\n", array[length - 1]);
}

/* O(n) */
void gen_rand_array(int *array, long length) {
    int i;
    for (i = 0; i < length; i++) {
        array[i] = rand() % LIMIT;
    }
}

/* O(n) */
void gen_sorted_array(int *array, long length) {
    int i;
    for (i = 0; i < length; i++) {
        array[i] = i;
    }
}

/* O(n) */
void gen_rev_sorted_array(int *array, long length) {
    int i;
    for (i = 0; i < length; i++) {
        array[i] = length-i;
    }
}

double time_sort(int *array, long length, void (*sort_fn)(int *array, long length)) {
    clock_t begin = clock();
    (*sort_fn)(array, length);
    clock_t end = clock();
    double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
    return time_spent;
}


double complexity_factor(long n, enum complexity cmpl) {
    switch (cmpl) {
        case N_SQ:
            return (double) n*n;
        case N_LG_N:
            return (double) n*log2(n);
        case LG_N:
            return (double) log2(n);
        case N:
            return (double) n;
    }
}

void print_stats(long *lengths, double *times, int number, enum complexity cmpl) {
    int i;
    double norm, result;
    printf("t is %f, l is %ld\n", times[1], lengths[1]);
    norm = (times[1] / complexity_factor(lengths[1], cmpl));
    printf("norm is %f\n", norm);
    for (i = 1; i < number; i++) {
        printf("Time taken for %10ld: % 3.6f", lengths[i], times[i]);
        result = (times[i] / complexity_factor(lengths[i], cmpl))/norm;
        printf(" %3.3f\n", result);
    }
}

int main(int argc, char **argv) {
    char c;
    int i;
    long length;
    int *array;
    long *lengths;
    double *times;
    void (*sort_fn)(int *array, long length);
    enum complexity cmpl;
    double time_spent;

    enum sort_type type;
    while ((c = getopt(argc, argv, "bihmq")) != -1) {
        switch (c) {
            case 'b':
                type = BIN_TREE;
                break;
            case 'i':
                type = INSERTION;
                break;
            case 'h':
                type = HEAP;
                break;
            case 'm':
                type = MERGE;
                break;
            case 'q':
                type = QUICK;
                break;
            default:
                printf("Incorrect sort type specified.\n");
                return 1;
        }
    }

    switch (type) {
        case INSERTION:
            sort_fn = ins_sort;
            cmpl = N_SQ;
            break;
        case BIN_TREE:
            sort_fn = bintree_sort;
            cmpl = N_LG_N;
            break;
        case HEAP:
            sort_fn = heap_sort;
            cmpl = N_LG_N;
            break;
        case MERGE:
            sort_fn = merge_sort;
            cmpl = N_LG_N;
            break;
        case QUICK:
            sort_fn = quick_sort;
            cmpl = N_LG_N;
            break;
    }

    srand(time(NULL));
    times = malloc(POWERS*sizeof(double));
    lengths = malloc(POWERS*sizeof(long));
    for (i = 1; i < POWERS; i++) {
        length = (int) pow(10, i);
        lengths[i] = length;
        array = calloc(length, sizeof(int));
        gen_rand_array(array, length);
        time_spent = time_sort(array, length, sort_fn);
        times[i] = time_spent;
        for (int j = 1; j < length; j++) {
            if (array[j] < array[j-1]) {
                printf("Invalid sort!\n");
                break;
            }
        }
        free(array);
    }
    print_stats(lengths, times, POWERS, cmpl);
    return 0;
}
