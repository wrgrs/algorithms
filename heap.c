#include<stdio.h>

/* Assume that the children of a[i] are max heaps. */
void simple_heapify(int *a, int i, long length) {
    int start = a[i];
    int child_1 = -1;
    int child_2 = -1;
    long child_1_index = i * 2 + 1;
    long child_2_index = child_1_index + 1;
    if (child_1_index < length) {
        child_1 = a[child_1_index];
    }
    if (child_2_index < length) {
        child_2 = a[child_2_index];
    }
    int child_1_bigger = child_1 > child_2;
    if (child_1_bigger && child_1 > start) {
        a[i] = child_1;
        a[child_1_index] = start;
        simple_heapify(a, child_1_index, length);
    } else if ( ! child_1_bigger && child_2 > start) {
        a[i] = child_2;
        a[child_2_index] = start;
        simple_heapify(a, child_2_index, length);
    }
}

/* Start from the last entries (which must be max heaps as they
 * have no children) and ensure that each heap we come to is a max
 * heap. */
void heapify(int *a, long length) {
    for (long i = length - 1; i >= 0; i--) {
        simple_heapify(a, i, length);
    }
}

/* Each time, create a max heap, then swap the first element (the new max)
 * for the last. Reduce the size of the heap accordingly. */
void heap_sort(int *a, long length) {
    heapify(a, length);
    for (long i = 0; i < length; i++) {
        simple_heapify(a, 0, length - i);
        int max = a[0];
        a[0] = a[length - i - 1];
        a[length - i - 1] = max;
    }
}


/*
int main() {
    long length = 7;
    long a[7] = {1, 2, 8, 3, 4, 3, 9};
    heapsort(a, leng    h);
    for (long i = 0; i < length; i++) {
        printf("Value %d: %d\n", i, a[i]);
    }
}
*/
