#include <stdio.h>

long partition(int *a, long start, long end) {
    long pivot = a[end];
    int i = start;
    int tmp = -1;
    for (int j = start; j < end; j++) {
        if (a[j] < pivot) {
            tmp = a[j];
            a[j] = a[i];
            a[i] = tmp;
            i++;
        }
    }
    tmp = a[end];
    a[end] = a[i];
    a[i] = tmp;
    return i;
}

void quick_sort_sub(int *a, long start, long end) {
    if (start < end) {
        int p = partition(a, start, end);
        quick_sort_sub(a, start, p-1);
        quick_sort_sub(a, p+1, end);
    }
}

void quick_sort(int *a, long length) {
    quick_sort_sub(a, 0, length - 1);
}
