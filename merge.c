#include <stdio.h>
#include <stdlib.h>


void merge(int *a, int start, int mid, int length) {
    int end = start + length;
    int first_index = start;
    int second_index = mid;
    int *tmp = calloc(sizeof(int), length);
    for (int idx = 0; idx < length; idx++) {
        if (!(first_index >= mid) && (second_index >= end || a[first_index] < a[second_index])) {
            tmp[idx] = a[first_index];
            first_index++;
        } else {
            tmp[idx] = a[second_index];
            second_index++;
        }
    }
    /* Copy the subarray back into the correct location. */
    for (int i = 0; i < length; i++) {
        a[start + i] = tmp[i];
    }
    free(tmp);
}


void split_and_merge(int *a, int start, int length) {
    int end = start + length;
    /* If length is one the subarray must be sorted. */
    if (length > 1) {
        int mid = (end + start) / 2;
        split_and_merge(a, start, mid - start);
        split_and_merge(a, mid, end - mid);
        merge(a, start, mid, end - start);
    }
}


void merge_sort(int *array, long length) {
    split_and_merge(array, 0, length);
}
